//
//  LoadingView.swift
//  WABOOKS_iOS_V2
//
//  Created by Mok Monita on 5/1/21.
//

import UIKit

public class LoadingView: UIView {
    
    var coachView                   : UIView!
    var spinner                     : UIActivityIndicatorView!
    
    private var coverView           : UIView?
    private var loadingImageView    : UIImageView?
    
    override public var frame : CGRect {
        didSet {
            self.update()
        }
    }
    
    class var sharedInstance: LoadingView {
        struct Singleton {
            static let instance = LoadingView(frame: CGRect(x: 0,y: 0, width: 58.0 ,height: 58.0))
        }
        
        return Singleton.instance
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.alpha = 0.0
        self.backgroundColor = UIColor.clear
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public class func show() {
        let currentWindow       : UIWindow = UIApplication.shared.keyWindow!
        
        let view                = LoadingView.sharedInstance
        view.backgroundColor    = UIColor.clear
        
        let height              : CGFloat = UIScreen.main.bounds.size.height
        let width               : CGFloat = UIScreen.main.bounds.size.width
        
        let center              : CGPoint = CGPoint(x: width / 2.0  , y: height / 2.0 )
        view.center             = center
        
        // SharedMethod.setStatusBarBackground(UIColor(red: 0, green: 0, blue: 0, alpha: 0.1))
        
        if view.superview == nil {
            view.coverView                  = UIView(frame: currentWindow.bounds)
            view.coverView?.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0)
            
            currentWindow.addSubview(view.coverView!)
            currentWindow.addSubview(view)
            
            UIView.animate(withDuration: 0.18, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                view.coverView?.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
            }, completion: nil)
            
            view.start()
        }
    }
    
    public class func hide(){
        let loadingView = LoadingView.sharedInstance
        loadingView.stop()
    }
    private func dismiss(completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: 0.3, animations: {
            LoadingView.hide()
        }) { _ in
            completion?(true)
        }
    }
    public class func delayBeforeHide(seconds: Double = 0.3, completion: ((Bool) -> Void)? = nil) {
        let time = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            LoadingView.sharedInstance.dismiss(completion: completion)
        }
    }
    
    private func start(){
        loadingImageView?.startAnimating()
        self.alpha = 1.0
    }
    
    private func stop(){
        DispatchQueue.main.async {
            if ((self.coverView?.superview) != nil) {
                self.coverView?.removeFromSuperview()
            }
            
            if self.superview != nil {
                self.removeFromSuperview()
            }
            
            self.loadingImageView?.stopAnimating()
            self.alpha = 0.0
        }
    }
    
    private func update() {
        if self.loadingImageView == nil {
            loadingImageView = UIImageView(frame: self.bounds)
            
            var images = [UIImage]()
            
            for index in 1...12 {
                let image = UIImage(named: String(format:"img_loading%02d",index))
                images.append(image!)
            }
            
            loadingImageView?.animationImages       = images
            loadingImageView?.animationDuration     = 1.0
            loadingImageView?.animationRepeatCount  = 0
            loadingImageView?.center                = CGPoint(x: 58 / 2, y: 58 / 2)
            
            self.addSubview(loadingImageView!)
        }
    }
}

