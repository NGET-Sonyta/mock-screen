//
//  ViewController.swift
//  MockScreen
//
//  Created by Nyta on 19/7/21.
//

import UIKit

class DetailVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    
    @objc func dismiss(_ sender:UIButton) {
       dismiss(animated: true, completion: nil)
    }
    
}


extension DetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return 1
        case 4:
            return 1
        case 5:
            return 1
        case 6:
            return 1
        case 7:
            return 1
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("indexPath.section", indexPath.section)
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! Cell1
            cell.img.image = UIImage(named: "kiwi")
            
            cell.btnBack.addTarget(self, action: #selector(self.dismiss(_:)), for: .touchUpInside)
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! Cell2
            cell.lblText.text = "Kiwi"
            
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! Cell2
            cell.lblText.text = "Per PC 50g approx"
            cell.lblText.font = UIFont.boldSystemFont(ofSize: 14)
            cell.lblText.textColor = #colorLiteral(red: 0.2000131607, green: 0.6754556894, blue: 0.3106464744, alpha: 1)
            
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! Cell2
            cell.lblText.text = "Kiwi fruit, or Chinese gooseberry, is the editable berry of serveral species of woody vines in the genus Actinidia. The most common ..."
            cell.lblText.font = UIFont(name: "Helvetica neue", size: 14)
            
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! Cell3
            
            return cell
            
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4") as! Cell4
            cell.lblDeliveryTime.text   = "Standard:" + "Friday Evening"
            cell.lblDeliveryTime.font       = UIFont.boldSystemFont(ofSize: 11)
            cell.lblDeliveryTime.textColor  = #colorLiteral(red: 0.6666069627, green: 0.6667048931, blue: 0.6665855646, alpha: 1)
            cell.lblDiscount.text       = "You save: 20%"
            cell.lblDiscount.font       = UIFont.boldSystemFont(ofSize: 11)
            cell.lblDiscount.textColor  = #colorLiteral(red: 0.2000131607, green: 0.6754556894, blue: 0.3106464744, alpha: 1)
            
            return cell
            
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell5") as! Cell5
            
            
            return cell
        default:
            return UITableViewCell()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 442
        case 1:
            return 70
        case 2:
            return 25
        case 3:
            return 30
        case 4:
            return 60
        case 5:
            return 30
        case 6:
            return 100
        case 7:
            return 100
        default:
            return 0
        }
    }
    
    
}

class Cell1 : UITableViewCell {
    
    @IBOutlet weak var img      : UIImageView!
    @IBOutlet weak var btnBack  : UIButton!
    

    
    @IBAction func actionViewCart(_ sender: Any) {
        
    }
    
    @IBAction func actionBack(_ sender: Any) {
        // Dismiss here
        
    }
    
}

class Cell2 : UITableViewCell {
    
    @IBOutlet weak var lblText: UILabel!
    
}

class Cell3 : UITableViewCell {
    
    
    @IBOutlet weak var lblPrice     : UILabel!
    @IBOutlet weak var lblQuantity  : UILabel!
    
    var quantity : Int = 1
    var price    : Float = 0.0
    
    
    @IBAction func actionPlus(_ sender: Any) {
        
        quantity = quantity + 1
        price    = price * 0.9
        
        lblQuantity.text = "\(quantity)"
        lblPrice.text    = "\(price)"
    }
    @IBAction func actionMinus(_ sender: Any) {
        
        quantity != 0 ? (quantity = quantity - 1) : (quantity = 0)
       
        price    = price / 0.9
        
        lblQuantity.text = "\(quantity)"
        lblPrice.text    = "\(price)"
    }
    
}

class Cell4 : UITableViewCell {
    
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    @IBOutlet weak var imgDelivery: UIImageView!
    
    
    
}

class Cell5: UITableViewCell {
    
    
    @IBAction func actionBuy(_ sender: Any) {
    }
    
    @IBAction func actionHeart(_ sender: Any) {
    }
    
}


