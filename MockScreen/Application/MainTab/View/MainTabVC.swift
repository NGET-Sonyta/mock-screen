//
//  MainTabVC.swift
//  MockScreen
//
//  Created by Nyta on 19/7/21.
//

import UIKit
import CarbonKit

class MainTabVC: UIViewController, CarbonTabSwipeNavigationDelegate {

    @IBOutlet weak var sideView: UIView!
    
    let items = ["Fruits", "Vegetables", "Nuts & Seeds", "Discount"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup Swipe Navigation
        setupCarbonTabSwipeNavigation()
        
    }
    
    //------set CarbonTabSwipeNavigation-------
    func setupCarbonTabSwipeNavigation() {
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)

        
        carbonTabSwipeNavigation.setNormalColor(UIColor.gray.withAlphaComponent(1.0), font: UIFont(name: "Helvetica Neue", size: 15) ?? UIFont(name: "", size: 15)!)

        // Color Selected
        carbonTabSwipeNavigation.setSelectedColor(UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1))
        
        // Color Indicator
        carbonTabSwipeNavigation.setIndicatorColor(UIColor(red: 50/255, green: 205/255, blue: 50/255, alpha: 1))
        
        // Background Color
        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        
        // Height Indicator
        carbonTabSwipeNavigation.setIndicatorHeight(3)
        
        // Height Carbon Segmented Control
        carbonTabSwipeNavigation.toolbarHeight.constant = 50
        
        

        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 4, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 4, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 4, forSegmentAt: 2)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 4, forSegmentAt: 3)
//        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: sideView)
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            let storyboard = UIStoryboard(name: "FruitsSB", bundle: nil)
            if #available(iOS 13.0, *) {
                return storyboard.instantiateViewController(identifier: "FruitsVC")
            } else {
                // Fallback on earlier versions
                return storyboard.instantiateViewController(withIdentifier: "FruitsVC")
            }
        }else if index == 1 {
           if #available(iOS 13.0, *) {
                return UIStoryboard(name: "VegetablesSB", bundle: nil).instantiateViewController(identifier: "VegetablesVC")
            } else {
                // Fallback on earlier versions
                return storyboard!.instantiateViewController(withIdentifier: "VegetablesVC")
            }
        }
        else if index == 2 {
        if #available(iOS 13.0, *) {
                return UIStoryboard(name: "NutsSB", bundle: nil).instantiateViewController(identifier: "NutsVC")
            } else {
                return UIStoryboard(name: "DiscountSB", bundle: nil).instantiateViewController(withIdentifier: "DiscountVC")
            }
        }else {
            if #available(iOS 13.0, *) {
                return UIStoryboard(name: "DiscountSB", bundle: nil).instantiateViewController(identifier: "DiscountVC")
            } else {
                // Fallback on earlier versions
                return storyboard!.instantiateViewController(withIdentifier: "DiscountVC")
            }
        }
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
     
        if index == 0 {
//            self.refreshButton.isHidden = false
//            navigationTitle.text = items[0]
//            arrImageViewController[Int(index)] = UIImage(contentsOfFile: "mn_credit_on_icon")
        }else if index == 1 {
//            self.refreshButton.isHidden = false
//            navigationTitle.text = arrNavigationTitle[1]
        }else if index == 2 {
//            self.refreshButton.isHidden = true
//            navigationTitle.text = arrNavigationTitle[2]
        }else {
//            self.refreshButton.isHidden = true
//            navigationTitle.text = arrNavigationTitle[3]
        }
        
    }
    
}
