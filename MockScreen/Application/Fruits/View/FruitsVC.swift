//
//  FruitsVC.swift
//  MockScreen
//
//  Created by Nyta on 19/7/21.
//

import UIKit

class FruitsVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var arrFruitTitle          =   ["Kiwi","Avocado","Papaya","Watermelon","Papaya","Watermelon"]
    var arrSubTitle            =   ["គីវី","ផ្លែប័រ","ល្ហុង","ឪឡឹក","ល្ហុង","ឪឡឹក"]
    var arrFruitPrice          =   ["0.9","0.8","0.5","1","3","4"]
//    var arrColor               =   [#colorLiteral(red: 0.9801751971, green: 0.7688937783, blue: 0.5976475477, alpha: 1) , #colorLiteral(red: 0.4533771873, green: 0.5731948018, blue: 0.3125351071, alpha: 1), #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1), #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)]
    var arrImage               =   [UIImage(named: "kiwi"),
                                                  UIImage(named: "kiwi"),
                                                  UIImage(named: "kiwi"),
                                                  UIImage(named: "kiwi"),
                                                  UIImage(named: "kiwi"),
                                                  UIImage(named: "kiwi")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customLayout()

    }
    func customLayout(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        let width = UIScreen.main.bounds.width
        layout.itemSize = CGSize(width: (width/2)-15 , height: width/1.8 )
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        collectionView!.collectionViewLayout = layout
    }
}

extension FruitsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFruitTitle.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        cell.title.text         = arrFruitTitle[indexPath.row]
        cell.subTitle.text      = arrSubTitle[indexPath.row]
        cell.price.text         = arrFruitPrice[indexPath.row] + "$"
        cell.img.image          = arrImage[indexPath.row]
//        cell.backgroundColor    = arrColor[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let storyboard = UIStoryboard(name: "DetailSB", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailVC")
//        modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
        
    }



}



class CollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var img      : UIImageView!
    @IBOutlet weak var title    : UILabel!
    @IBOutlet weak var subTitle : UILabel!
    @IBOutlet weak var price    : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
